#ifndef __TWIX_H
#define __TWIX_H

#include <avr/io.h>
#include <avr/interrupt.h>

/*! \brief Definition of pin used as SDA. */
#ifndef SDA
#define SDA PB1
#endif

/*! \brief Definition of pin used as SCL. */
#ifndef SCL
#define SCL PB0 
#endif

#ifndef LED_PIN
#define LED_PIN PB3 // pb3 => pin 2
#endif

/*! \brief Definition of 7 bit slave address. */
#ifndef SLAVE_ADDRESS
#define SLAVE_ADDRESS 0x5D
#endif


#define INITIALIZE_SDA()        ( DDRB &= ~ (1 << SDA) )    //!< Clear port. 
#define READ_SDA()              ( (PINB & (1 << SDA)?1:0) )       //!< read pin value
#define READ_SCL()              ( (PINB & (1 << SCL))?1:0 )

//SDA and SCL bit in PORT variable must be 0!
#define SETSDA()  ( DDRB &= ~(1 << SDA) ) //Tristate
#define CLRSDA()  ( DDRB |= (1 << SDA) )  //Pull it low

#define SETSCL()  ( DDRB &= ~(1 << SCL) ) //Tristate
#define CLRSCL()  ( DDRB |= (1 << SCL) )  //Pull it low

/* External interrupt macros. These are device dependent. */
#define INITIALIZE_TWI_INTERRUPT()    (MCUCR |= (1<<ISC01))  //!< Sets falling edge of SDA generates interrupt.
#define ENABLE_TWI_INTERRUPT()        (GIMSK |= (1<<INT0))   //!< Enables SDA interrupt.
#define DISABLE_TWI_INTERRUPT()       (GIMSK &= ~(1<<INT0))  //!< Disables SDA interrupt.
#define CLEAR_TWI_INTERRUPT_FLAG()    (GIFR = (1<<INTF0))    //!< Clears interrupt flag.

#define SDA_vector  INT0_vect  //!< Interrupt vector for SDA pin

// Dedicated general purpose registers. 
register unsigned char TWSR asm("r2");;
register unsigned char TWDR asm("r3");;
register unsigned char TWEA asm("r4");;

//! \name macros for twi state machine
//! @{
# define TWI_SLA_REQ_W_ACK_RTD              0x60    
# define TWI_SLA_DATA_RCV_ACK_RTD           0x80
# define TWI_SLA_DATA_RCV_NACK_RTD          0x88

# define TWI_SLA_REQ_R_ACK_RTD              0xA8
# define TWI_SLA_DATA_SND_ACK_RCV           0xB8
# define TWI_SLA_DATA_SND_NACK_RCV          0xC0
# define TWI_SLA_LAST_DATA_SND_ACK_RCV      0xC8

# define TWI_SLA_REPEAT_START               0xA0
# define TWI_SLA_STOP                       0x68
# define I2C_IDLE                           0x00
//! @}

unsigned char readI2Cslavebyte(void);
void twi_slave_init( void );
void twi_slave_enable( void );

void twi_slave_disable( void );
void receivedata(void);
void senddata(void);
void TWI_state_machine(void);
void GetStartCondition(void);


/* Global variables */
unsigned char incomingBuffer[2]; //!< Incoming buffer, accomodates address and write command
unsigned char outgoingBuffer[2] = {LASER_OFF,SLAVE_ADDRESS}; //!< Preloaded outgoing buffer for testing - may be modified by outer code


#define COMMAND_NONE 0
#define COMMAND_WRITE 1 
#define COMMAND_READ 2
#define COMMAND_COMPLETE 4 
unsigned char commandState = COMMAND_NONE;

/*! \brief initialize twi slave mode
 */
void twi_slave_init( void )
{
    INITIALIZE_SDA(); 
    INITIALIZE_TWI_INTERRUPT();
    TWEA = 1;
    
    TWSR = I2C_IDLE;
}


/*! \brief enable twi slave
 */
void twi_slave_enable( void )
{
  CLEAR_TWI_INTERRUPT_FLAG();
  ENABLE_TWI_INTERRUPT();
}


/*! \brief disable twi slave
 */
void twi_slave_disable( void )
{
  DISABLE_TWI_INTERRUPT();
  CLEAR_TWI_INTERRUPT_FLAG();
}


/*! \brief Interrupt service routine for negative egde on SDA
 */
ISR(SDA_vector) {
  volatile unsigned char retval;
  
  if(TWSR == I2C_IDLE)
  {
    GetStartCondition();
  }
  
  //!Call the TWi state machine
  TWI_state_machine();
  CLEAR_TWI_INTERRUPT_FLAG();
  ENABLE_TWI_INTERRUPT();

  
}

/*! \brief Read the slave byte after start condition
 */
inline unsigned char readI2Cslavebyte(void)
{
  unsigned char index = 0;
  unsigned char val = 0;
  unsigned char cPin = 0;
  
  
  //Let SCL go low first. MCU comes here while SCL is still high
  while(READ_SCL());
  
  //!read 8 bits from master, respond with ACK.
  //!SCL could be high or low depending on CPU speed
  for(index = 0;index < 8; index++)
  {
    
    while(!READ_SCL());
    cPin = READ_SDA();
    
    val = (val<<1) | cPin;
    while(READ_SCL())
    {
      //PORTB ^= (1<<(PB4)); //timing debug
      //if SDA changes while SCL is high, it indicates STOP or START
      if((val & 1)!= cPin)
      {
        if(READ_SDA())
          TWSR = TWI_SLA_STOP;
        else
          TWSR = TWI_SLA_REPEAT_START;
        return 0;
        //!return READ_SDA()?I2C_STOP_DETECTED:I2C_START_DETECTED;
      } 
      else
        cPin = READ_SDA();
      //PORTB ^= (1<<(PB4)); //timing debug
    }
  }
  
  //!Send ACK, SCL is low now
  if((val & 0xFE) == (SLAVE_ADDRESS << 1))
  {
    
    CLRSDA();
    while(!READ_SCL());
    while(READ_SCL());
    SETSDA();
    CLRSCL(); //!!!clock_stretching start
  }
  else
  {
    TWSR = I2C_IDLE;
    return 0;
  }
  return val; 
}

/*! \brief TWI slave send data
 */
inline void senddata(void)
{
  unsigned char index;
  for(index = 0;index < 8; index++)
  {
    while(READ_SCL());
    //PORTB ^= (1<<(PB4));
    if((TWDR >> (7 - index))&1)
      SETSDA();
    else
      CLRSDA();
    //PORTB ^= (1<<(PB4)); //timing debug
    SETSCL(); //!!! clock_stretching stop
    while(!READ_SCL());
  }
  //!See if we get ACK or NACk
  while(READ_SCL());
  
  //!tristate the pin to see if ack comes or not
  SETSDA();
  
  while(!READ_SCL());
  if(!READ_SDA())
    TWSR = TWI_SLA_DATA_SND_ACK_RCV;
  else
    TWSR = TWI_SLA_DATA_SND_NACK_RCV;
}


/*! \brief TWI slave receive data
 */
inline void receivedata(void)
{
  unsigned char index;
  TWDR = 0;
  SETSCL(); //!!! clock_stretching stop
  
  //!read 8 bits from master, respond with ACK.
  //!SCL could be high or low depending on CPU speed
  for(index = 0;index < 8; index++)
  {
    while(!READ_SCL());
    TWDR = (TWDR<<1) | READ_SDA();
    while(READ_SCL())
    {
      //!if SDA changes while SCL is high, it indicates STOP or START
      if((TWDR & 1)!= READ_SDA())
      {
        if(READ_SDA())
          TWSR = TWI_SLA_STOP;
        else
          TWSR = TWI_SLA_REPEAT_START;
        return;
      } 
    }
  }
  
  if(TWEA)
  { 
    //!Send ACK, SCL is low now
    CLRSDA();
    while(!READ_SCL());
    while(READ_SCL());
    SETSDA();
    TWSR = TWI_SLA_DATA_RCV_ACK_RTD;
    TWEA = 0;
  }
  else
  {
    TWSR = TWI_SLA_DATA_RCV_NACK_RTD;
  }
  
  
  
}

/*! \brief Identify start condition
 */
inline void GetStartCondition(void)
{
  unsigned char retval = 0;
  //!Make sure it is the start by checking SCL high when SDA goes low
  if(READ_SCL())
  {
    DISABLE_TWI_INTERRUPT();
  }
  else //!false trigger; exit the ISR
  {
    CLEAR_TWI_INTERRUPT_FLAG();
    ENABLE_TWI_INTERRUPT();
    return;
  }
  //!lop for one or several start conditions before a STOP
  if(TWSR == I2C_IDLE || TWSR == TWI_SLA_REPEAT_START)
  {
    retval = readI2Cslavebyte(); //!read address
    if(retval == 0)//!STOP or otehr address received
    {
      TWSR = I2C_IDLE;
      CLEAR_TWI_INTERRUPT_FLAG();
      ENABLE_TWI_INTERRUPT();
      return;
    }
    else
    {
      if(retval & 1)
        TWSR = TWI_SLA_REQ_R_ACK_RTD;
      else
        TWSR = TWI_SLA_REQ_W_ACK_RTD;
    }
  }
  TWDR = retval;
  
}



/*! \brief TWI state machine software algorithm that emulates the hardware TWI state machine.
 */
void TWI_state_machine(void)
{
    //! get current state
START: 
    //! \brief lock twi task              
    switch (TWSR) 
    {    
        /*! Own SLA_W has been received; 
         *! ACK has been returned
         */
        case TWI_SLA_REQ_W_ACK_RTD: 
            incomingBuffer[0] = TWDR; 
            commandState = COMMAND_WRITE;
            TWEA = 1;
            receivedata();
            goto START;
            break;
            
        //! data recieved, NACK has been returned
        case TWI_SLA_DATA_RCV_NACK_RTD:
            TWSR = I2C_IDLE;
            TWEA = 1;
            break;
        
        //! data recieved, ack has been returned
        case TWI_SLA_DATA_RCV_ACK_RTD:
            incomingBuffer[1] = TWDR;
            TWEA = 1;
            commandState |= COMMAND_COMPLETE;
            receivedata();
            goto START;
            break;


        /*! Own SLA_R has been received; 
         *! ACK has been returned
         */
        case TWI_SLA_REQ_R_ACK_RTD:
            //PORTB ^= (1<<(PB4)); //timing debug
            TWDR = outgoingBuffer[0];
            TWEA = 1;
            commandState = COMMAND_READ;
            senddata();
            goto START;
            break;

        //! data has been transmitted, ACK has been received.
        case TWI_SLA_DATA_SND_ACK_RCV:
            TWDR = outgoingBuffer[1];
            TWEA = 1;
            commandState |= COMMAND_COMPLETE;
            senddata();
            goto START;
            break;
            
        //! last data has been transmitted, ACK has been received.
        case TWI_SLA_LAST_DATA_SND_ACK_RCV:
         
        //! data has been transmitted, NACK has been received.
        case TWI_SLA_DATA_SND_NACK_RCV:
            TWEA = 1;
            TWSR = I2C_IDLE;
            
            break;

        //! met stop or repeat start
        case TWI_SLA_STOP:
              //! return to idle state
              TWEA = 1;
              TWSR = I2C_IDLE;
          
            break;
        case TWI_SLA_REPEAT_START:  
          GetStartCondition();
          goto START;
          
        //! Idle or bus error
        case I2C_IDLE:
        default:
            TWEA = 1; 
            break;        
    }
}


#endif //__TWI_H


