#define MODULE_NUMBER 1
#define SLAVE_ADDRESS (0x5D + MODULE_NUMBER)
#define SDA PB1
#define SCL PB0 
#define LED_PIN PB3 // pb3 => pin 2

#define LASER_OFF 0xbe
#define LASER_ON 0x3c

#include "TWI.h"

void setup()
{
    pinMode(LED_PIN, OUTPUT);

    CLKPR =(1<<CLKPCE);
    CLKPR =0; // set 9.6 MHz CPU freq

    sei();   //!< Enable global interrupts.

    twi_slave_init();
    twi_slave_enable();   
}


void loop()
{
    if (TWSR == I2C_IDLE && (commandState & COMMAND_COMPLETE))
    {
        // the read command can be executed after some delay    
        if (commandState && COMMAND_WRITE)
        {
            if (incomingBuffer[1] == LASER_OFF)
            {
                outgoingBuffer[0] = LASER_OFF;
                digitalWrite(LED_PIN, LOW);
            }
            else
            {
                outgoingBuffer[0] = LASER_ON;
                digitalWrite(LED_PIN, HIGH);           
            }
        }

        commandState = COMMAND_NONE;       
    }
}
