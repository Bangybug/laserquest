#ifndef _SOFTI2C_H
#define _SOFTI2C_H   1

#include <avr/io.h>
#include <Arduino.h>

#pragma GCC diagnostic push

#pragma GCC diagnostic ignored "-Wunused-parameter"


void __attribute__ ((noinline)) i2c_init(void) __attribute__ ((used));
void __attribute__ ((noinline)) i2c_wait_sclsda_high(void) asm("i2c_wait_sclsda_high")  __attribute__ ((used));
void __attribute__ ((noinline)) i2c_start_wait(void) asm("i2c_start_wait")  __attribute__ ((used));


#ifndef I2C_ADDRESS
#define I2C_ADDRESS 0x40
#endif

// map the IO register back into the IO address space
#define SDA_DDR         (_SFR_IO_ADDR(SDA_PORT) - 1)
#define SCL_DDR         (_SFR_IO_ADDR(SCL_PORT) - 1)
#define SDA_OUT         _SFR_IO_ADDR(SDA_PORT)
#define SCL_OUT         _SFR_IO_ADDR(SCL_PORT)
#define SDA_IN    (_SFR_IO_ADDR(SDA_PORT) - 2)
#define SCL_IN    (_SFR_IO_ADDR(SCL_PORT) - 2)

#ifndef __tmp_reg__
#define __tmp_reg__ 0
#endif
 
// call at the beginning
void i2c_init(void)
{
  __asm__ __volatile__ 
    (" cbi      %[SDADDR],%[SDAPIN]     ;release SDA \n\t"
     " cbi      %[SCLDDR],%[SCLPIN]     ;release SCL \n\t" 
     " cbi      %[SDAOUT],%[SDAPIN]     ;clear SDA output value \n\t"     
     " cbi      %[SCLOUT],%[SCLPIN]     ;clear SCL output value \n\t"
     " ret "
     : :
     [SCLDDR] "I"  (SCL_DDR), [SCLPIN] "I" (SCL_PIN), 
     [SCLIN] "I" (SCL_IN), [SCLOUT] "I" (SCL_OUT),
     [SDADDR] "I"  (SDA_DDR), [SDAPIN] "I" (SDA_PIN), 
     [SDAIN] "I" (SDA_IN), [SDAOUT] "I" (SDA_OUT)); 
}


// wait until bus is free (sda=1, scl=1)
void i2c_wait_sclsda_high(void)
{
  __asm__ __volatile__ 
    ("_Li2c_wait_stretch1: \n\t"
     " sbis %[SCLIN],%[SCLPIN]  ;wait for SCL high \n\t" 
     " rjmp _Li2c_wait_stretch1 \n\t"
     " sbis %[SDAIN],%[SDAPIN]  ;wait for SDA high \n\t" 
     " rjmp _Li2c_wait_stretch1 \n\t"
     " ret "
     : : 
     [SCLIN] "I" (SCL_IN), 
     [SCLPIN] "I" (SCL_PIN),
     [SDAIN] "I" (SDA_IN),
     [SDAPIN] "I" (SDA_PIN));
}


// wait until transfer is started (sda=0, scl=1)
void i2c_start_wait(void)
{
  __asm__ __volatile__ 
    ("_Li2c_wait_stretch2: \n\t"
     " sbis %[SCLIN],%[SCLPIN]  ;wait for SCL high \n\t" 
     " rjmp _Li2c_wait_stretch2 \n\t"
     " sbic %[SDAIN],%[SDAPIN]  ;wait for SDA low \n\t" 
     " rjmp _Li2c_wait_stretch2 \n\t"
     " ret "
     : : 
     [SCLIN] "I" (SCL_IN), 
     [SCLPIN] "I" (SCL_PIN),
     [SDAIN] "I" (SDA_IN),
     [SDAPIN] "I" (SDA_PIN));
}


uint8_t i2c_read()
{
  // bits being read into carry flag (clc - clears carry flag, sec - sets carry flag)
  __asm__ __volatile__ 
  (
    "I2c_wait_for_start: \n\t"
    " rcall i2c_start_wait \n\t"
    "I2c_get_1:                ;get 1st byte\n\t" 
    "I2c_10: \n\t" 
    " sbic %[SCLIN],%[SCLPIN]  ;wait for SCL low \n\t" 
    " rjmp I2c_10 \n\t" 
    " ldi r22,8                ;bits to receive=8\n\t" 
    "I2c_11: \n\t" 
    " sbis %[SCLIN],%[SCLPIN]  ;wait for SCL high \n\t" 
    " rjmp I2c_11 \n\t" 
    " sbic %[SDAIN],%[SDAPIN]  ;check next SDA bit \n\t" 
    " rjmp I2c_13 \n\t" 
    "I2c_12:                   ;if SDA=0 \n\t" 
    " sbic %[SDAIN],%[SDAPIN]  \n\t" 
    " rjmp I2c_wait_for_start  ;SDA 0->1? I2CSTOP! (unexpected: wait for next start) \n\t"
    " sbic %[SCLIN],%[SCLPIN]  ;wait for SCL low \n\t" 
    " rjmp I2c_12              ;loop while SCL=1 \n\t"
    " clc                      ;SDA=0->C\n\t"
    " rjmp I2c_15 \n\t"
    "I2c_13:                   ;if SDA=1\n\t"
    " sbis %[SDAIN],%[SDAPIN]  \n\t" 
    " rjmp I2c_get_1           ;SDA 1->0? I2CSTART! (repeated start) \n\t"
    " sbic %[SCLIN],%[SCLPIN]  ;wait for SCL low \n\t" 
    " rjmp I2c_13              ;loop while SCL=1 \n\t" 
    " sec                      ;SDA=1->C \n\t" 
    "I2c_15: \n\t"
    " rol r24                  ;carry flag is inserted into bit0 and the r24 gets shifted \n\t"
    " dec r22  \n\t"
    " brne I2c_11              ;loop to next bit \n\t"
    " ret "
    : : 
    [SCLIN] "I" (SCL_IN), 
    [SCLPIN] "I" (SCL_PIN),
    [SDAIN] "I" (SDA_IN),
    [SDAPIN] "I" (SDA_PIN)
  );
  return ' '; // never gets here
}



#pragma GCC diagnostic pop

#endif
