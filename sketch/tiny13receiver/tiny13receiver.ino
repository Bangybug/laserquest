// https://create.arduino.cc/projecthub/taunoerik/programming-attiny13-with-arduino-uno-07beba
// file/preferences/additional boards urls
// https://mcudude.github.io/MicroCore/package_MCUdude_MicroCore_index.json
// ATTiny13A

#define SDA_PORT PORTB
#define SDA_PIN PB0  //pb0 => pin 5
#define SCL_PORT PORTB
#define SCL_PIN PB1  //pb1 => pin 6
#define I2C_FASTMODE 0

// arduino nano: SDA - a4, SCL - a5

#define I2C_HOST_SLAVE_ADDR 2

#include "SoftI2CMaster.h"

#define TOLERANCE 7
#define PIN_PHOTODIODE A2

bool light = false;
int current = 0;

void setup() 
{
    delay(1000);
    i2c_init();
}


void loop() 
{
    int val = analogRead(PIN_PHOTODIODE);
    if (current != 0)
    {
        int diff = val - current;
        if (abs(diff) > TOLERANCE)
        {
            if ((diff < 0) != light)
            {
                light = !light;
                i2c_start_wait( (I2C_HOST_SLAVE_ADDR<<1) | I2C_WRITE);
                i2c_write(light);
                i2c_stop();
            }
        }
    }
    current = val;

    delay(20);
} 