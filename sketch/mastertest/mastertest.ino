#include <Wire.h>

#define I2C_SLAVE_ADDR 2
#define PIN_BUTTON_INTERRUPT 2

volatile bool shouldChange = false;
unsigned long buttonDebounce;
bool state = false;

void setup()
{
    pinMode(PIN_BUTTON_INTERRUPT, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(PIN_BUTTON_INTERRUPT), onButtonPressed, CHANGE);

    Wire.begin();

    Serial.begin(9600);
}


void loop()
{
    if (shouldChange && millis() - buttonDebounce < 300)
    {
        shouldChange = false;
        state = !state;

        Wire.beginTransmission(I2C_SLAVE_ADDR);
        Wire.write( state ? 1 : 0 );
        Wire.endTransmission();

        Serial.println("Button clicked");
    }
}


void onButtonPressed() 
{
    if (!shouldChange)
    {
        shouldChange = true;
        buttonDebounce = millis();
    }
}