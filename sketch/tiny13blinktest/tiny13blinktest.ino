#define LED_PIN PB3 // pb3 => pin 2

unsigned long waitTime;
unsigned long pDelay = 100;
byte state = LOW;

void setup() 
{
    pinMode(LED_PIN, OUTPUT);
    waitTime = millis() + pDelay;
}


void loop() 
{
    if (millis() > waitTime)
    {
        waitTime = millis() + pDelay;

        if (state == LOW)
            state = HIGH;
        else 
            state = LOW;

        digitalWrite(LED_PIN, state);
    }
} 