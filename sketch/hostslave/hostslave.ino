#include <Wire.h> 

#define I2C_ADDR 2

void setup() 
{ 
    // Wire will enable internal pullups
    Wire.begin(I2C_ADDR);

    // disable internal pullups
    digitalWrite(SDA, LOW); 
    digitalWrite(SCL, LOW);

    Wire.onReceive(onReceive);
    Serial.begin(9600);
} 

void loop() 
{ 
    delay(100); 
} 


void onReceive(int numBytes)
{
    while(Wire.available()) 
    {
        // read byte as a character
        int c = Wire.read(); 
        Serial.print("Signal from receiver No.");
        Serial.println(c);
    }
}
