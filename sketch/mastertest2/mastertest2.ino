#include <Wire.h> 

#define I2C_SLAVE 0x5d

void setup() 
{ 
    // Wire will enable internal pullups
    Wire.begin();
    //Wire.setClock(10000);

    // disable internal pullups
    digitalWrite(SDA, LOW); 
    digitalWrite(SCL, LOW);

    Serial.begin(9600);
    Serial.println("Test");
} 

void loop() 
{ 
    Wire.beginTransmission(I2C_SLAVE);
    Wire.write(0x3d);
    Wire.endTransmission();

    delay(1000); 

/*
    Wire.requestFrom(I2C_SLAVE, 1);
    while(Wire.available())    // slave may send less than requested
    { 
        uint8_t c = Wire.read();    // receive a byte as character
        Serial.println(c);         // print the character
    }    
*/

    delay(1000); 
} 

