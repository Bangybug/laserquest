// https://create.arduino.cc/projecthub/taunoerik/programming-attiny13-with-arduino-uno-07beba
// file/preferences/additional boards urls
// https://mcudude.github.io/MicroCore/package_MCUdude_MicroCore_index.json
// ATTiny13A

#define SDA_PORT PORTB
#define SDA_PIN PB0  //pb0 => pin 5
#define SCL_PORT PORTB
#define SCL_PIN PB1  //pb1 => pin 6
#define I2C_FASTMODE 0

// arduino nano: SDA - a4, SCL - a5

#define LED_PIN PB4 // pb4 => pin 3
#define I2C_HOST_SLAVE_ADDR 2

#include "SoftI2CMaster.h"

unsigned long waitTime;
byte state = LOW;
bool good = false;

void setup() 
{
    pinMode(LED_PIN, OUTPUT);

    digitalWrite(LED_PIN, HIGH);
    delay(1000);
 
    if (i2c_init())
    {
        good = true;
        digitalWrite(LED_PIN, LOW);
    }

    waitTime = millis() + 1000;
}


void loop() 
{
    if (good && millis() > waitTime)
    {
        waitTime = millis() + 1000;
        i2c_start_wait( (I2C_HOST_SLAVE_ADDR<<1) | I2C_WRITE);
        i2c_write(state);
        i2c_stop();

        if (state == LOW)
            state = HIGH;
        else 
            state = LOW;
        digitalWrite(LED_PIN, state);
    }
} 