#include <Wire.h> 
#include <SerialCommand.h>

#define VERSION 1
#define I2C_SLAVE_LASER_START 0x5d

#define LASER_OFF 0xbe
#define LASER_ON 0x3c

#define SENSOR_PIN A0


SerialCommand SCmd;
const char *ERR = "ERR";
const char *OK = "OK";

uint8_t sensorState = LOW;
unsigned long waitTime;
unsigned long pDelay = 100;
bool debounce = false;

void setup() 
{ 
    // Wire will enable internal pullups
    Wire.begin();

    // disable internal pullups
    digitalWrite(SDA, LOW); 
    digitalWrite(SCL, LOW);

    delay(500); 

    SCmd.addCommand("LASER", commandLaser);

    Serial.begin(9600);
    Serial.print("Firmware version ");
    Serial.println(VERSION);

    waitTime = millis() + pDelay;
    pinMode(SENSOR_PIN, INPUT);
} 


bool decodeIntArgs( int num, int buff[] )
{
    bool err = false;
    for (int i=0; i<num; ++i)
    {
        char *arg = SCmd.next(); 
        if (arg) 
        {
            buff[i] = atoi(arg);
        }
        else
        {
            err = true;
            break;
        }
    }
    return !err;
}


void commandLaser()
{
    int args[2];
    if (decodeIntArgs(2, args))
    {
        doLaser(args[0], args[1] == 1);
        Serial.println(OK);
    }
    else
    {
        Serial.println(ERR);
    }        
}

void doLaser(int n, bool enabled)
{
    uint8_t state = enabled ? LASER_ON : LASER_OFF;
    Wire.beginTransmission(I2C_SLAVE_LASER_START + n);
    Wire.write(state);
    Wire.endTransmission();
}


void loop() 
{ 
    SCmd.readSerial();

    if (!debounce || millis() > waitTime)
    {
        if (sensorState != digitalRead(SENSOR_PIN))
        {
            debounce = true;
            waitTime = millis() + pDelay;
            sensorState = sensorState == LOW ? HIGH : LOW;
            doLaser(0, sensorState == HIGH);
        }
        else
        {
            debounce = false;
        }
    }
} 

