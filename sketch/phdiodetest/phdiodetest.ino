#define PIN_PHOTODIODE A2

#define TOLERANCE 7
int current = 0;
bool light = false;

void setup() 
{ 
    Serial.begin(9600);
} 



void loop() 
{ 
    int val = analogRead(PIN_PHOTODIODE);
    if (current != 0)
    {
        int diff = val - current;
        if (abs(diff) > TOLERANCE)
        {
            if ((diff < 0) != light)
            {
                if (light)
                    Serial.println("dark");
                else
                    Serial.println("light");
                light = !light;
            }
        }

    }
    current = val;

    delay(50);
} 


