package dashboard;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class TemperatureTest
{
    public static final String EVENT_TEMPERATURE_READ = "temperature-read";
    public final EventSource eventSource = new EventSource();
    
    private Object sleeper = new Object();
    private CountDownLatch shutdownLatch;
    private volatile boolean enabled = false;
    private boolean hasThread = false;
    private boolean shutdown = false;
    
    private int maxTemperature = 50;
    private int minTemperature = 17;

    private void runThread()
    {

        try
        {
            while (!shutdown)
            {
                synchronized (sleeper)
                {
                    try 
                    {
                        if (!enabled)
                        {
                            sleeper.wait();
                        }
                        else
                        {
                            long sleepTime = 500;
                            long sleepStart = System.currentTimeMillis();
                            sleeper.wait(sleepTime);
                            long slept = System.currentTimeMillis() - sleepStart;
                            
                            if (!enabled)
                                continue;
                            
                            advance(slept);
                        }
                    } 
                    catch (InterruptedException e) 
                    {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        }
        finally
        {
            shutdownLatch.countDown();
            hasThread = false;
        }
    }

    
    
    public void enable(boolean enabled)
    {
        synchronized (sleeper)
        {
            this.enabled = enabled;

            if (!hasThread)
            {
                if (enabled)
                    createThread();
            }
            else
                sleeper.notify();
        }
    }
    
    private void createThread()
    {
        shutdownLatch = new CountDownLatch(1);
        hasThread = true;
        Thread thread = new Thread(){
            public void run() 
            {
                runThread();
            }
        };
        thread.start();
    }
    
    public void stop()
    {
        enable(false);
    }
    
    public void shutdown()
    {
        if (hasThread)
        {
            synchronized (sleeper) 
            {
                shutdown = true;
                enabled = false;
                sleeper.notify();
                try 
                {
                    shutdownLatch.await(500, TimeUnit.MILLISECONDS);
                } 
                catch (InterruptedException e) 
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private void advance(long slept)
    {
        eventSource.fireEventAsync(EVENT_TEMPERATURE_READ, (int)( Math.random() * (maxTemperature - minTemperature) ) + minTemperature );
    }
    

}
