package dashboard;

public class DeviceState 
{
    public static transient int CURRENT_FORMAT = 1;
    
    public int formatVersion = 0;
    public boolean programRunning;
    public boolean[] motorEnabled = new boolean[16];  
    public int[] motorValues = new int[ 16 * 7 ];
    public ToneDuration[] sequence = new ToneDuration[0];
    public boolean desync = true;
    
    
    public static final class ToneDuration 
    {
        int tone;
        int duration;
        
        public ToneDuration() {}
        
        public ToneDuration(int tone, int duration)
        {
            this.tone = tone;
            this.duration = duration;
        }
        
        public static int unpackDuration(int dur)
        {
            int units = dur & 0x3;
            int duration = 1 + (dur >> 2);
            if (units == 0)
                duration *= 100;
            else if (units == 1)
                duration *= 1000;
            else if (units == 2)
                duration *= 60000;
            else if (units == 3)
                duration *= 3600000;
            return duration;    
        }
        
        public static int packDuration(int dur)
        {
            int units = 0;
            int ret = 0;
            if (dur / 100 < 65)
            {
                ret = dur / 100;
                if (0 == ret)
                    ret = 1;
            }
            else if (dur / 1000 < 65)
            {
                units = 1;
                ret = dur / 1000;
            }
            else if (dur / 60000 < 65)
            {
                units = 2;
                ret = dur / 60000;
            }
            else if (dur / 3600000 < 65)
            {
                units = 3;
                ret = dur / 3600000;
            }
            else 
            {
                units = 3;
                ret = 64;
            }
            return units | ((ret-1) << 2);
        }
    }
    
    
    public boolean isMotorEnabled(int motorNumber)
    {
        return motorEnabled[motorNumber];
    }
    
    public void setMotorEnabled(int motorNumber, boolean enabled)
    {
        motorEnabled[motorNumber] = enabled;
    }
    
    public int getMotorValue(int motorNumber, int tone)
    {
        return motorValues[motorNumber*7 + tone];
    }
    
    public void setMotorValue(int motorNumber, int tone, int motorValue)
    {
        motorValues[motorNumber*7 + tone] = motorValue;
    }
    
    public void enableAll(boolean enabled)
    {
        for (int i=0; i<motorEnabled.length; ++i)
            setMotorEnabled(i, enabled);
    }
}
