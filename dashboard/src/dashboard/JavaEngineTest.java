package dashboard;

import java.util.ArrayList;
import java.util.List;

public class JavaEngineTest extends JavaEngine implements IEventListener
{
    private DeviceState currentState = new DeviceState();
   
    
    public JavaEngineTest()
    {
        currentState.enableAll(false);

        currentState.setMotorEnabled(0, true);
        currentState.setMotorValue(0, 0, 300);
        currentState.setMotorValue(0, 1, 500);
        currentState.setMotorValue(0, 2, 750);
        currentState.setMotorValue(0, 3, 800);
        
        currentState.setMotorEnabled(1, false);
        currentState.setMotorValue(0, 0, 900);
        
        currentState.setMotorEnabled(2, true);
        currentState.setMotorValue(2, 0, 3999);
        
        currentState.sequence = new DeviceState.ToneDuration[]{
                new DeviceState.ToneDuration(0, 3 * 1000),
                new DeviceState.ToneDuration(1, 2 * 1000),
                new DeviceState.ToneDuration(2, 1 * 1000)
        };
        
        
    }
    
    public void connect(String portName)
    {
        JavaScriptEngine.getInstance().connected();
    }
    
    protected List<String> getPortNames()
    {
        ArrayList<String> portList = new ArrayList<>();
        portList.add("Fake-port");
        return portList;
    }
    
    protected DeviceState getDeviceState()
    {
        return currentState;
    }
    
    
    public void disconnect()
    {
        JavaScriptEngine.getInstance().disconnected();
    }
    
    
    public void enableMotor(int motorIndex, boolean enabled)
    {
        currentState.setMotorEnabled(motorIndex, enabled);
    }
    
    public void setMotorValue(int motorIndex, int tone, int value)
    {
        currentState.setMotorValue(motorIndex, tone, value);
    }

    public void setDeviceSettings(DeviceState settings)
    {
        currentState = settings;
        JavaScriptEngine.getInstance().setCurrentState();
    }

    public void queryDeviceState() 
    {
        JavaScriptEngine.getInstance().setCurrentState();
    }

    @Override
    public void storeDeviceSettings() 
    {
        // N/A
    }

    @Override
    public void handleEvent(String eventType, Object arg) 
    {
    }

}
