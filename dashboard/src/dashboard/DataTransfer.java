package dashboard;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import com.fazecast.jSerialComm.SerialPort;

public class DataTransfer
{
    public static final String EVENT_COMMAND_SENDING_STARTED = "command_sending";
    public static final String EVENT_COMMAND_SENDING_DONE = "command_sending_done";
    public static final String EVENT_COMMAND_RESULT = "cmdresult";
    public static final String EVENT_DISCONNECTED = "disconnected";
    public static final String EVENT_INITIALIZED = "init";
    public static final String EVENT_INCOMING_DATA = "incoming_data";
    
    public static final String COMMAND_INIT = "INIT";
    public static final String COMMAND_DISCONNECT = "DISC";
    public static final String COMMAND_LOAD_SETTINGS = "LSE";
    public static final String COMMAND_SAVE_SETTINGS = "SSE";
    public static final String COMMAND_ENABLE_MOTOR = "ME";
    public static final String COMMAND_SET_MOTOR_TONE_VALUE = "MV";
    public static final String COMMAND_SEQUENCE_RUN = "SS";
    public static final String COMMAND_SEQUENCE_UPDATE = "SU";
    
    private String pendingCommand;
    private ArrayList<String> pendingCommands = new ArrayList<String>();

    public final class CommandResultEvent
    {
        public String command;
        public boolean isOk;
        public String[] responseTokens;
    }

    public static char INCOMING_DATA_TEMPERATURE = 'T';
    public static char INCOMING_DATA_SEQUENCE = 'S';
    
    public final class IncomingDataEvent 
    {
        public String data;
        public char dataType;
    }


    public final EventSource eventSource = new EventSource();
    
    // short-duration lock used to deliver port closure signal to the thread
    private Object portOperationLock = new Object();

    // short-duration lock to synchronize command sending and data retrieval
    private Object sendCommandLock = new Object();
    private boolean commandsSending = false;

    
    private SerialPort port;
    
    private final byte[] errPattern;
    private final byte[] okPattern;
    private final byte[] incomingDataPattern;
    
    public DataTransfer()
    {
        errPattern = "ERR".getBytes();
        okPattern = "OK".getBytes();
        incomingDataPattern = "DT".getBytes();
    }
    
    
    public void executeInit()
    {
        executeCommand(COMMAND_INIT, "");
    }
    
    public void executeDisconnect()
    {
        executeCommand(COMMAND_DISCONNECT, "");
    }
    
    public void executeLoadSettings()
    {
        executeCommand(COMMAND_LOAD_SETTINGS, "");
    }
    
    public void executeSaveSettings()
    {
        executeCommand(COMMAND_SAVE_SETTINGS, "");
    }
    
    public void executeMotorEnable(int motorIndex, boolean enabled)
    {
        executeCommand(COMMAND_ENABLE_MOTOR, String.format("%d %d", motorIndex, enabled ? 1 : 0));
    }
    
    public void executetMotorValue(int motorIndex, int toneIndex, int value)
    {
        executeCommand(COMMAND_SET_MOTOR_TONE_VALUE, String.format("%d %d %d", motorIndex, toneIndex, value));
    }

    public void executeSequence(boolean enable)
    {
        executeCommand(COMMAND_SEQUENCE_RUN, enable ? "1" : "0");
    }
    
    public void executeSaveSequence(DeviceState.ToneDuration []sequence, boolean desync)
    {
        executeCommand(COMMAND_SEQUENCE_UPDATE, String.format("%d %d %d",0,desync ? 1 : 0, 0));
        for (int i=0; i<sequence.length; ++i)
            executeCommand(COMMAND_SEQUENCE_UPDATE, String.format("%d %d %d", 1, sequence[i].tone, DeviceState.ToneDuration.packDuration(sequence[i].duration)));
    }

    private void executeCommand(String command, String args) throws IllegalStateException
    {
        synchronized(sendCommandLock)
        {
            if (null == port)
                throw new IllegalStateException();
            
            pendingCommands.add(command + " " + args + "\r");
            
            if (!commandsSending)
            {
                eventSource.fireEventAsync(EVENT_COMMAND_SENDING_STARTED, true);
                commandsSending = true;
                sendNextCommand();
            }
        }
    }
    
    private void sendNextCommand()
    {
        if (pendingCommands.isEmpty())
        {
            commandsSending = false;
            if (null != pendingCommand)
            {
                eventSource.fireEventAsync(EVENT_COMMAND_SENDING_DONE, "");
                pendingCommand = null;
            }
        }
        else
        {
            String command = pendingCommands.get(0);
            pendingCommand = command.substring(0, command.indexOf(" "));
            pendingCommands.remove(0);
            byte[] commandBytes = command.getBytes();
            port.writeBytes( commandBytes, commandBytes.length);
        }
    }


    /**
     * Creates the thread for data transfer. 
     * Assumes that you don't call it while you already have an open port.
     * @param port
     */
    public void onPortOpened(SerialPort port) throws IllegalStateException
    {
        if (this.port != null)
            throw new IllegalStateException();
        
        commandsSending = false;
        
        // clear queues, etc
        pendingCommand = null;
        pendingCommands.clear();
        
        this.port = port;
        createThread(port);
    }
    
    public boolean closePort()
    {
        // request the current thread to end and close the port (if any)
        boolean ret = false;
        synchronized (portOperationLock) 
        {
            ret = this.port != null;
            this.port = null;
        }
        return ret;
    }

    private void fireDisconnect()
    {
        DataTransfer.this.port = null;
        try {
            eventSource.fireEventSync(EVENT_DISCONNECTED, true);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }       
    }


    private void createThread(final SerialPort port)
    {
        // create a new thread that listens for incoming data
        Thread thread = new Thread(){
            public void run() 
            {
                byte[] data = new byte[16384];
                CircularBuffer stack = new CircularBuffer(1024 * 10);
                
                while (true) 
                {
                    // port closed by some reason
                    if (!port.isOpen())
                    {
                        fireDisconnect();
                        return;
                    }
                    
                    // fetch data - we have to do it asap, otherwise Arduino's buffer may overflow
                    synchronized (sendCommandLock)
                    {
                        InputStream is = port.getInputStream();
                        try 
                        {
                            int available = Math.min( is.available(), data.length );
                            if (0 != available)
                                available = is.read(data, 0, available);

                            if (0 != available)
                            {               
                                // once the command is sent, we discard all pending data before we have the response
                                checkPendingCommandResponse(stack, data, available);
                            }
                        } 
                        catch (Exception e1) 
                        {
                            e1.printStackTrace();
                            fireDisconnect();
                        }
                    }

                    // close port requested?
                    synchronized (portOperationLock) 
                    {
                        if (null == DataTransfer.this.port)
                        {
                            port.closePort();
                            fireDisconnect();
                            return;
                        }
                    }
                    
                }
            }
        };
        thread.start();     
    }
    
    
    
    private void checkPendingCommandResponse(CircularBuffer stack, byte[] data, int count)
    {
        // we are looking for patterns "OK<zero-or more>\r" or "ERR<zero or more>\r"
        // for simplicity lets accumulate up to N bytes on a circular stack, and when we see \r, we check the accumulated bytes for the pattern
        
        for (int i=0; i<count; ++i)
        {
            if (data[i] == '\r')
            {
                
                // command response processing
                int responseIndex = stack.reverseFindFirst(okPattern); 
                if (-1 == responseIndex)
                    responseIndex = stack.reverseFindFirst(errPattern);
                if (-1 != responseIndex)
                {
                    String response = new StringBuilder( stack.copyToStringReverse(responseIndex) ).reverse().toString();
                    processCommandResponse(response);
                }
                
                // incoming data processing
                responseIndex = stack.reverseFindFirst(incomingDataPattern);
                if (-1 != responseIndex)
                {
                    String incomingData = new StringBuilder( stack.copyToStringReverse(responseIndex) ).reverse().toString();
                    processIncomingData(incomingData);
                }
                
                stack.reset();
            }
            else
            {
                stack.put(data[i]);
            }
        }
    }
    
    private void processIncomingData(String incomingData)
    {
        IncomingDataEvent event = new IncomingDataEvent();
        event.dataType = incomingData.charAt(incomingDataPattern.length);
        event.data = incomingData.substring(incomingDataPattern.length + 1);
        eventSource.fireEventAsync(EVENT_INCOMING_DATA, event);
    }
    
    private void processCommandResponse(String response)
    {
        boolean isOk = false;
        if (response.startsWith("OK"))
        {
            response = response.substring(2);
            isOk = true;
        }
        else if (response.startsWith("ERR"))
        {
            response = response.substring(3);
        }
        String[] tokens = response.split(" ");
        CommandResultEvent event = new CommandResultEvent();
        event.command = pendingCommand;
        event.isOk = isOk;
        event.responseTokens = tokens;
        sendNextCommand();
        eventSource.fireEventAsync(EVENT_COMMAND_RESULT, event);
    }

}
