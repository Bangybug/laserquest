package dashboard;

import com.fazecast.jSerialComm.SerialPort;

import dashboard.DataTransfer.CommandResultEvent;

public class JavaEngineArduino extends JavaEngine implements IEventListener
{
    private boolean isConnected = false;
    private boolean isDisconnecting = false;
    private DataTransfer dataTransfer = new DataTransfer();
    private DeviceState currentState = new DeviceState();
    
    private static final int BAUD_RATE = 9600;

    public JavaEngineArduino()
    {
        dataTransfer.eventSource.addListener(DataTransfer.EVENT_DISCONNECTED, this);
        dataTransfer.eventSource.addListener(DataTransfer.EVENT_COMMAND_RESULT, this);
        dataTransfer.eventSource.addListener(DataTransfer.EVENT_INCOMING_DATA, this);
    }
    
    public void handleEvent(String eventType, Object arg) 
    {
        if (eventType == DataTransfer.EVENT_DISCONNECTED)
        {
            isConnected = false;
            isDisconnecting = false;
            JavaScriptEngine.getInstance().disconnected();
        }
        else if (eventType == DataTransfer.EVENT_COMMAND_RESULT)
        {
            CommandResultEvent commandResult = (CommandResultEvent)arg;
            
            if (!commandResult.isOk)
            {
                disconnect();
                return;
            }
            
            if (commandResult.command.equals(DataTransfer.COMMAND_INIT))
                JavaScriptEngine.getInstance().connected();
            else if (commandResult.command.equals(DataTransfer.COMMAND_DISCONNECT))
            {
                isDisconnecting = true;
                dataTransfer.closePort();
            }
            
            if (isDisconnecting)
                return;
            
            if (commandResult.command.equals(DataTransfer.COMMAND_LOAD_SETTINGS))
            {
                parseSettings(commandResult.responseTokens);
                JavaScriptEngine.getInstance().setCurrentState();
            }
            else if (commandResult.command.equals(DataTransfer.COMMAND_SAVE_SETTINGS))
            {
                JavaScriptEngine.getInstance().debug("Settings saved");
            }
        }
        else if (eventType == DataTransfer.EVENT_INCOMING_DATA)
        {
            DataTransfer.IncomingDataEvent event = (DataTransfer.IncomingDataEvent)arg;
            if (event.dataType == DataTransfer.INCOMING_DATA_TEMPERATURE)
                JavaScriptEngine.getInstance().updateTemperature( (int)Float.parseFloat( event.data ) );
            else if (event.dataType == DataTransfer.INCOMING_DATA_SEQUENCE)
            {
                String[] data = event.data.split(" ");
                JavaScriptEngine.getInstance().setMotorTone( Integer.parseInt(data[0]), Integer.parseInt(data[1]));
            }
        }
    }
    

    private void parseSettings(String[] tokens)
    {
        if (tokens.length >= 16*7+16)
        {
            int i = 0;
            for (int motorIndex=0; motorIndex<16; ++motorIndex)
            {
                for (int toneIndex=0; toneIndex<7; ++toneIndex)
                {
                    currentState.setMotorValue(motorIndex, toneIndex, Integer.parseInt(tokens[i]));
                    ++i;
                }
            }
            for (int motorIndex=0; motorIndex<16; ++motorIndex)
            {
                currentState.setMotorEnabled(motorIndex, 1 == Integer.parseInt(tokens[i]));
                ++i;
            }
            
            
            int sequenceLength = Integer.parseInt(tokens[i++]);
            currentState.desync = Integer.parseInt(tokens[i++]) == 1;
            currentState.sequence = new DeviceState.ToneDuration[sequenceLength];
            for (int j=0; j<sequenceLength; ++j)
            {
                currentState.sequence[j] = new DeviceState.ToneDuration();
                currentState.sequence[j].tone = Integer.parseInt(tokens[i++]);
                currentState.sequence[j].duration = DeviceState.ToneDuration.unpackDuration( Integer.parseInt(tokens[i++]) );
            }
            
            JavaScriptEngine.getInstance().debug("Settings loaded");
        }
        else
        {
            JavaScriptEngine.getInstance().debug("Bad token length -- "+tokens.length);
        }
    }

    @Override
    public void connect(String portName)
    {
        if (!isConnected && !isDisconnecting)
        {
            isDisconnecting = false;
            
            SerialPort chosenPort = SerialPort.getCommPort( portName );
            chosenPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
            chosenPort.setBaudRate(BAUD_RATE);
            if(chosenPort.openPort()) 
            {
                isConnected = true;
                dataTransfer.onPortOpened(chosenPort);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dataTransfer.executeInit();
            }
        }
    }

    @Override
    public void disconnect()
    {
        if (isConnected && !isDisconnecting)
        {
            isDisconnecting = true;
            dataTransfer.executeDisconnect();
        }
    }
    
    public void queryDeviceState() 
    {
        if (isConnected && !isDisconnecting)
            dataTransfer.executeLoadSettings();
    }


    @Override
    protected DeviceState getDeviceState()
    {
        return currentState;
    }

    @Override
    protected void setDeviceSettings(DeviceState settings)
    {
        currentState = settings;
        JavaScriptEngine.getInstance().setCurrentState();
        
        if (isConnected && !isDisconnecting)
        {
            for (int motorIndex=0; motorIndex<16; ++motorIndex)
            {
                dataTransfer.executeMotorEnable(motorIndex, currentState.isMotorEnabled(motorIndex) );
                for (int toneIndex=0; toneIndex<7; ++toneIndex)
                {
                    int value = currentState.getMotorValue(motorIndex, toneIndex);
                    dataTransfer.executetMotorValue(motorIndex, toneIndex, value); 
                }
            }
            
            dataTransfer.executeSaveSequence(settings.sequence, settings.desync);
        }
    }
    
    public void storeDeviceSettings()
    {
        if (isConnected && !isDisconnecting)
        {
            setDeviceSettings(getDeviceState());
            dataTransfer.executeSaveSettings();
        }
    }
    
}
