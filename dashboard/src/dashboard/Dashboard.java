package dashboard;

import java.io.File;
import java.net.URL;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import netscape.javascript.JSObject;

public class Dashboard extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception
    {
        stage.setMaximized(true);
        Scene scene = new Scene(new Group());

        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();
        
        //browser.setBlendMode(BlendMode.DARKEN);

        webEngine.getLoadWorker().stateProperty()
                .addListener((ChangeListener<State>) (observable, oldValue, newValue) -> {
                    if (newValue == Worker.State.SUCCEEDED)
                        stage.setTitle(webEngine.getTitle());
                });

        // process page loading
        webEngine.getLoadWorker().stateProperty()
                .addListener((ObservableValue<? extends State> ov, State oldState, State newState) -> {
                    if (newState == State.SUCCEEDED)
                    {
                        JSObject win = (JSObject) webEngine.executeScript("window");
                        win.setMember("engine", JavaEngine.getInstance());
                        JavaScriptEngine.getInstance().setWebEngine(webEngine);
                        JavaScriptEngine.getInstance().init();
                    }
                });


        String currentDir = System.getProperty("user.dir");
        System.out.println("Current directory: "+currentDir);
        
        URL url = Dashboard.class.getResource("html/dashboard.html");
        if (null == url)
            url = new File(currentDir+File.separator+"src"+File.separator+"dashboard"+File.separator+"html"+File.separator+"dashboard.html").toURI().toURL();
        
        if (null == url)
            System.out.println("Cannot find dashboard.html");
        else
        {
            System.out.println("Loading "+url.toExternalForm());
        
            webEngine.load(
                    url.toExternalForm()
                    );
    
            scene.setRoot(browser);

            stage.setScene(scene);
            stage.show();
        }
        
        System.out.println(webEngine.getUserAgent());
        
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                JavaEngine.getInstance().disconnect();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        
    }


}