package dashboard;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.fazecast.jSerialComm.SerialPort;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public abstract class JavaEngine
{
    private static JavaEngine __engine = null;
    
    private GsonBuilder builder = new GsonBuilder();
    
    public synchronized static JavaEngine getInstance()
    {
        if (null == __engine)
        {
            String test = System.getenv("DASHBOARD_TEST");
            if (test != null && !test.isEmpty())
                __engine = new JavaEngineTest();
            else
                __engine = new JavaEngineArduino();
        }
        return __engine;
    }
    
    
    public void exit()
    {
        Platform.exit();
    }
    
    
    public abstract void connect(String portName);
    
    public abstract void disconnect();
    
    public abstract void queryDeviceState(); 
    
    protected abstract DeviceState getDeviceState();
    
    protected abstract void setDeviceSettings(DeviceState settings);
    
    public abstract void storeDeviceSettings();
    
    public String queryPorts()
    {
        List<String> portList = new ArrayList<>();
        SerialPort[] portNames = SerialPort.getCommPorts();
        for(int i = 0; i < portNames.length; i++)
            portList.add(portNames[i].getSystemPortName());
        return builder.create().toJson(portList);
    }
    
      
    private void showFileWriteError()
    {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("������");
        alert.setHeaderText("���� �� ����� ���� �������");
        alert.setContentText("�������� ������ �����.");
        alert.showAndWait();        
    }
    
    private void showFileReadError()
    {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("������");
        alert.setHeaderText("���� �� ����� ���� ��������");
        alert.setContentText("������ ������ ��� �������� ������ �����");
        alert.showAndWait();        
    }
    
    
    public void saveSettingsToFile(String settings)
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new ExtensionFilter("Vibrodriver (*.lsr)","lsr"));
        fileChooser.setTitle("��������� ���");
        File file = fileChooser.showSaveDialog(null);
        
        if (null == file)
            return;

        if(!file.getName().contains(".vbr"))
            file = new File(file.getAbsolutePath() + ".lsr");

        if (!file.canWrite() && file.exists())
        {
            showFileWriteError();
        }
        else
        {
            try
            {
                PrintWriter writer = new PrintWriter(file, "UTF-8");
                writer.print(settings);
                writer.close();
            } 
            catch (IOException e) 
            {
                showFileWriteError();
            }
        }
    }
    
    
    public void loadSettingsFromFile()
    {
        FileChooser fileChooser = new FileChooser();
        ExtensionFilter filter = new ExtensionFilter("Laserquest (*.lsr)","*.lsr");
        fileChooser.getExtensionFilters().add( filter );
        fileChooser.setSelectedExtensionFilter(filter);
        fileChooser.setTitle("��������� ���������");
        File file = fileChooser.showOpenDialog(null);

        if (null == file)
            return;
        
        if (!file.canRead())
        {
            showFileReadError();
        }
        else
        {
            Gson gson = new Gson();
            try
            {
                DeviceState state = gson.fromJson(new FileReader(file), DeviceState.class);
                setDeviceSettings(state);
            } 
            catch (Exception e)
            {
                e.printStackTrace();
                showFileReadError();
            } 
        }
    }
    
    
    
    
}
