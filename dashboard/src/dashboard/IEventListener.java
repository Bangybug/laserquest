package dashboard;

public interface IEventListener 
{

    void handleEvent(String eventType, Object arg);
    
    
}
