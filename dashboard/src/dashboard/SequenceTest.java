package dashboard;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import dashboard.DeviceState.ToneDuration;

public class SequenceTest 
{
    public static final String EVENT_TONE_STARTED = "tone-started";
    public static final String EVENT_SEQUENCE_STOPPED = "stopped";
    public final EventSource eventSource = new EventSource();
    
    public static final class ToneEvent
    {
        int tone;
        int motor;
        int percent;
    }

    private Object sleeper = new Object();
    private CountDownLatch shutdownLatch;
    private volatile boolean enabled = false;
    private boolean hasThread = false;
    private ToneDuration[] sequence;
    
    private long totalDuration;
    private long currentDuration;
    
    private boolean shutdown = false;
    
    
    private static final class MotorState 
    { 
        int motorIndex;
        int cursor;
        long duration;
    }
    
    private MotorState[] motorStates;
    
    
    public void play(boolean motorEnabled[], ToneDuration[] sequence, boolean desync)
    {
        synchronized (sleeper)
        {
            int enabledMotors = 0;
            for (boolean b : motorEnabled)
                if (b)
                    ++enabledMotors;
            
            if (enabledMotors == 0 || sequence.length == 0)
            {
                enable(false);
            }
            else
            {
                this.sequence = sequence.clone();
                totalDuration = calculateTotalDuration(sequence);
                currentDuration = 0;
                
                motorStates = new MotorState[enabledMotors];
                
                int nextEnabledMotor = 0;
                
                for (int i=0; i<motorStates.length; ++i)
                {
                    MotorState ms = motorStates[i] = new MotorState();
                    for (; !motorEnabled[nextEnabledMotor++]; );
                    ms.motorIndex = nextEnabledMotor - 1;
                    if (desync && i > 0)
                        ms.cursor = Math.max(sequence.length-1, (int) Math.floor( sequence.length * Math.random() ));
                    ms.duration = sequence[ms.cursor].duration;
                    playTone(ms.motorIndex, sequence[ms.cursor].tone);
                }

                enable(true);
            }
        }
    }
    
    public void stop()
    {
        enable(false);
    }
    
    public void shutdown()
    {
        if (hasThread)
        {
            synchronized (sleeper) 
            {
                shutdown = true;
                enabled = false;
                sleeper.notify();
                try 
                {
                    shutdownLatch.await(500, TimeUnit.MILLISECONDS);
                } 
                catch (InterruptedException e) 
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    private void playTone(int motorIndex, int toneIndex)
    {
        ToneEvent te = new ToneEvent();
        te.motor = motorIndex;
        te.tone = toneIndex;
        te.percent = (int) (100.0f * (float)currentDuration / totalDuration);
        eventSource.fireEventAsync(EVENT_TONE_STARTED, te);
    }
    
    private long calculateTotalDuration(ToneDuration[] sequence)
    {
        long ret = 0;
        for (int i=0; i<sequence.length; ++i)
            ret += sequence[i].duration;
        return ret;
    }
    
    
    private void enable(boolean enabled)
    {
        synchronized (sleeper)
        {
            this.enabled = enabled;

            if (!hasThread)
            {
                if (enabled)
                    createThread();
            }
            else
                sleeper.notify();
        }
    }
    
    private void createThread()
    {
        shutdownLatch = new CountDownLatch(1);
        hasThread = true;
        Thread thread = new Thread(){
            public void run() 
            {
                runThread();
            }
        };
        thread.start();
    }

    
    
    
    private void advance(long slept)
    {
        currentDuration += slept;
        
        for (int i=0; i<motorStates.length; ++i)
        {
            MotorState ms = motorStates[i];
            ms.duration -= slept;
            if (ms.duration <= 0)
            {
                if (++ms.cursor == sequence.length)
                {
                    if (i == 0)
                        currentDuration = 0;
                    ms.cursor = 0;
                }
                ms.duration = sequence[ms.cursor].duration;
                playTone(ms.motorIndex, sequence[ms.cursor].tone);
            }
        }
    }
    
    
    private long calculateMinSleepTime()
    {
        long ret = -1;
        for (MotorState ms : motorStates)
            ret = ret == -1 ? ms.duration : Math.min(ret, ms.duration);
        return ret;
    }
    
    
    
    private void runThread()
    {

        try
        {
            while (!shutdown)
            {
                synchronized (sleeper)
                {
                    try 
                    {
                        if (!enabled)
                        {
                            eventSource.fireEventAsync(EVENT_SEQUENCE_STOPPED, null);
                            sleeper.wait();
                        }
                        else
                        {
                            long sleepTime = calculateMinSleepTime();
                            long sleepStart = System.currentTimeMillis();
                            sleeper.wait(sleepTime);
                            long slept = System.currentTimeMillis() - sleepStart;
                            
                            if (!enabled)
                                continue;
                            
                            advance(slept);
                        }
                    } 
                    catch (InterruptedException e) 
                    {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        }
        finally
        {
            shutdownLatch.countDown();
            hasThread = false;
        }
    }
    
}
