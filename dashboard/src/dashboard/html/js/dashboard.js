function externalCall(funcName, param)
{
    APP.Dashboard[funcName].apply( APP.Dashboard, [param] );
}


var APP = (function(APP, $)
{
    if (typeof APP === 'undefined') APP = {};
    if (typeof APP.Dashboard === 'undefined') APP.Dashboard = {};

    var connected = false;
    var $portsContainer;
    var deviceState;

    function debug(str)
    {
        var $debug = $("#debug");
        $debug.text(str);
    }

    APP.Dashboard.debug = function(str)
    {
        debug(str);
    };

    APP.Dashboard.init = function()
    {
        debug( window.engine ? "Init ok" : "Init errror" );
        window.engine.queryPorts();
    };

    APP.Dashboard.setPortNames = function(portNames)
    {
        var $portsContainer = $("#action-connect .dropdown-content");
        $("#tmpl-ports").tmpl( 
        {
            ports: portNames.map(function(portName) {
                return {id: portName, name:portName}
            })
        } ).appendTo( $portsContainer.empty() );
        $('#no-port').click( function(e){
            window.engine.queryPorts();
        });
        ports = portNames;
        $portsContainer.find('.connect-to-port').click(function(e){
            window.engine.connect( ports[ $(this).index() ] );
            $portsContainer.addClass('disabled');
        });
    };

    APP.Dashboard.connected = function()
    {
        connected = true;
        $portsContainer.addClass('disabled');
        $('#action-connect').addClass('connected').find(".dropbtn").text("Подключено");
        debug("Подключено");
    };

    APP.Dashboard.connectionFailed = function()
    {
        connected = false;
        $portsContainer.removeClass('disabled');
        $('#action-connect').removeClass('connected');
        debug("Подключение не удалось");
    };

    APP.Dashboard.disconnected = function()
    {
        connected = false;
        $portsContainer.removeClass('disabled');
        $('#action-connect').removeClass('connected').find(".dropbtn").text("Подключиться");
        debug("Отключено");
    };

    function __parseNumber(str)
    {
        var ret = null;
        if (typeof str === 'string')
            str = str.replace(',','.');
        var intn = parseInt(str);
        if (intn == str)
            ret = intn;
        else ret = parseFloat(str);
        if (isNaN(ret))
            return null;
        return ret;
    }


    if (!window.engine)
    {
        window.engine = 
        {
            queryPorts: function() {},
            connect: function(portId) {},
            disconnect: function() {}
        };
    }


    $(document).ready(function()
    {
        $portsContainer = $("#action-connect .dropdown-content");

        $('#action-connect .dropbtn').click( function(e) {
            if (connected)
                engine.disconnect();
        });
    });


    return APP;
})(APP, jQuery);
