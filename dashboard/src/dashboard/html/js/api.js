var APP = (function(APP, $)
{
    if (typeof APP === 'undefined') APP = {};
    if (typeof APP.Api === 'undefined') APP.Api = {
        initialized: false,
        engine: {
            selectDirectory: function(onSuccess) {
            },
            log: function(text) { 
                console.log(text);
            },
            openFolder: function() {},
            queryPorts: function() { return []; }
        }
    };

    var prodConfig = {
        init: false,
        selectDirectory: false
    };

    var testConfig = {
        init: true,
        selectDirectory: true,
        queryPorts: true
    }

    var test = testConfig;

    APP.Api.selectDirectory = function(onSuccess)
    {
        if (test.selectDirectory)
        {
            window.setTimeout(  function(){
                var directory = "d:/java/apache-tomcat-9.0.8"
                onSuccess( directory )
            }, 1000);
        }
        else
        {
            APP.Api.engine.selectDirectory(onSuccess);
        }
    }


    APP.Api.initEngine = function()
    {
        try
        {
            if (window.api)
            {
                APP.Api.engine.selectDirectory = function(onSuccess) { 
                    APP.UI.setResultHandler("selectDirectory", onSuccess);
                    window.api.selectDirectory();
                }

                APP.Api.engine.log = function(text) {
                    window.api.log(text);
                }

                APP.Api.engine.queryPorts = function() {
                    return JSON.parse( window.api.queryPorts() );
                }
            }
        }
        catch (e) {
        }    

        APP.Api.initialized = true;
        APP.UI.onResult("init");
    }

    APP.Api.log = function(text)
    {
        APP.Api.engine.log(text);
    }


    APP.Api.init = function(onSuccess)
    {
        if (test.init)
            onSuccess();
        else
        {
            if (APP.Api.initialized)
                onSuccess();
            else
                APP.UI.setResultHandler("init",onSuccess);
        }
    }


    APP.Api.queryPorts = function()
    {
        if (test.queryPorts)
        {
            return ["Test-COM1"]
        }
        else
        {
            return APP.Api.engine.queryPorts();
        }
    }


    return APP;
})(APP, jQuery);