function externalCall(funcName, param, param1)
{
    if (!APP || !APP.UI)
        setTimeout( function(){
            externalCall(funcName, param)
        }, 100 );
    else
    {
        APP.UI[funcName].apply( APP.UI, [param,param1] );
    }
}

var APP = (function(APP, $)
{
    if (typeof APP === 'undefined') APP = {};
    if (typeof APP.UI === 'undefined') APP.UI = {
        compInfo: {}
    };

    var steps = [
        "sensors"
    ];

    var currentStep = "sensors";
    var stepHistory = [currentStep];

    var $progress;
    var fileSeparator = "/";


    APP.UI.resultHandlers = {
        "initEngine": function() {
            APP.Api.initEngine();
        }
    };

    APP.UI.onResult = function(callName, resultText)
    {
        if (APP.UI.resultHandlers[callName])
            APP.UI.resultHandlers[callName]( resultText );
    };

    APP.UI.setResultHandler = function(callName, func)
    {
        APP.UI.resultHandlers[callName] = func;
    }

    function __showCurrentStep()
    {
        $("div.step").hide();
        $("div.step."+currentStep).show(400);
        err(false);
    }

    function showStep(step)
    {
        if (currentStep !== step)
        {
            currentStep = step;
            stepHistory.push(step);
            __showCurrentStep();
            APP.Api.engine.log("Step "+step);
        }
    }

    function back()
    {
        if (stepHistory.length > 1)
        {
            currentStep = stepHistory[stepHistory.length-2];
            if (currentStep === "tomcat-details")
                refreshSelectedTomcat();
            APP.Api.engine.log("back to "+currentStep);
            stepHistory.splice( stepHistory.length-1,1 );
            __showCurrentStep();
        }
    }

    APP.UI.progress = function(enable)
    {
        if (enable)
            $progress.show();
        else
            $progress.hide();
    }

    function err(text)
    {
        var $err = $("#error");
        if (text)
            $err.show().text(text).get(0).scrollIntoView();
        else 
            $err.hide();
    }


    function setPortNames(portNames)
    {
        var $portsContainer = $("#ports");
        $("#tmpl-ports").tmpl( 
        {
            ports: portNames.map(function(portName) {
                return {id: portName, name:portName}
            })
        } ).appendTo( $portsContainer.empty() );
        $portsContainer.change( function() {
            var portId = $portsContainer.children("option:selected").val()
            window.engine.connect( portId );
            $portsContainer.addClass('disabled');
        });
    }


    $(document).ready(function() 
    {
        APP.Api.init(function(){
            setPortNames( APP.Api.queryPorts() )
        });
    });

    return APP;
})(APP, jQuery);