package dashboard;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javafx.scene.web.WebEngine;

public class JavaScriptEngine
{
    private static JavaScriptEngine __engine = null;
    private WebEngine webEngine;
    
    public synchronized static JavaScriptEngine getInstance()
    {
        if (null == __engine)
            __engine = new JavaScriptEngine();
        return __engine;
    }
    
    public void setWebEngine(WebEngine webEngine)
    {
        this.webEngine = webEngine;
    }

    public void init()
    {
    	webEngine.executeScript("externalCall('onResult','initEngine')");
    }

    
    public void connected()
    {
    	webEngine.executeScript("externalCall('onResult','connected')");
    }
    
    public void disconnected()
    {
    	webEngine.executeScript("externalCall('onResult','disconnected')");
    }
 
    public void setCurrentState()
    {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        webEngine.executeScript("externalCall('setDeviceState',"+gson.toJson(JavaEngine.getInstance().getDeviceState())+")");
    }
    
    public void debug(String str)
    {
        webEngine.executeScript("externalCall('debug','"+str+"')");
    }
    
    public void setMotorTone(int motorIndex, int toneIndex)
    {
        webEngine.executeScript("externalCall('setMotorTone',["+motorIndex+","+toneIndex+"])");
    }
    
    public void updateTemperature(int degrees)
    {
        webEngine.executeScript("externalCall('updateTemperature',"+degrees+")");
    }
    
}
